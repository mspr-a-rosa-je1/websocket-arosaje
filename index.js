const express = require('express');
const http = require('http');
const { Server } = require('socket.io');
const { v4: uuidv4 } = require('uuid');
require('dotenv').config();

const port = process.env.PORT || 5000;
const backendUrl = `${process.env.BACKEND_HOST}:${process.env.BACKEND_PORT}`;

const app = express();
const httpServer = http.createServer(app);
const io = new Server(httpServer, { cors: { origin: '*' } });

const clients = {};

httpServer.listen(port, () => {
   console.log(`Server listening on port ${port}`);
});

// Middleware
io.use(async (socket, next) => {
   const { id, contactId, jwt } = socket.handshake.query;

   console.log(`Received connection request with id=${id}, contactId=${contactId}`);

   if (!id || !contactId || !jwt) {
      console.log('Data missing:', id, contactId, jwt);
      return next(new Error('Missing data for connection'));
   }

   console.log(`Validating connection for contactId=${contactId}`);

   try {
      const fetch = (await import('node-fetch')).default;
      const res = await fetch(`${backendUrl}/api/v1/messages/validate_connection/${contactId}`, {
         method: 'GET',
         headers: {
            "Authorization": `Bearer ${jwt}`,
         },
      });

      if (res.status === 200) {
         next();
      } else {
         next(new Error('Authentication refused by server, possibly due to wrong data'));
      }
   } catch (error) {
      console.error('Fetch error:', error);
      next(new Error('Fetch failed'));
   }
});

io.on('connection', (client) => {
   const { id, contactId, jwt } = client.handshake.query;
   console.log(`New connection: id=${id}, contactId=${contactId}`);

   clients[id] = client.id;

   client.on('message', async (msg) => {
      console.log(`Message received from ${id} to ${contactId}: ${msg.content}`);
      try {
         const fetch = (await import('node-fetch')).default;
         const res = await fetch(`${backendUrl}/api/v1/messages/${contactId}`, {
            method: 'POST',
            headers: {
               "Authorization": `Bearer ${jwt}`,
               "Content-Type": "application/json",
            },
            body: JSON.stringify({
               content: msg.content,
            }),
         });

         if (res.status !== 201) {
            console.error('Something went wrong while saving the message');
            return;
         }

         io.to(client.id).emit('message', msg);
         clients[contactId] && io.to(clients[contactId]).emit('message', msg);
      } catch (error) {
         console.error('Fetch error:', error);
      }
   });

   client.on('writing', () => {
      console.log(`User ${id} is typing a message to ${contactId}`);
      clients[contactId] && io.to(clients[contactId]).emit('writing');
   });

   client.on('notWriting', () => {
      console.log(`User ${id} stopped typing to ${contactId}`);
      clients[contactId] && io.to(clients[contactId]).emit('notWriting');
   });

   client.on('disconnect', (reason) => {
      console.log(`Client ${id} disconnected due to ${reason}`);
      if(clients[id]) {
         delete clients[id];
      }
   });
});
