# Websocket Arosaje

Dépot GitLab du projet websocket Arosaje. Projet étudiant dans le cadre d'une MSPR. Ce projet est le développement d'un websocket pour Arosaje, qui permet de au utilisateur de communiquer via un live chat.

## Installation

Clonez le dépôt.

Node est requis sur votre machine pour installer le websocket.

Puis exécutez les commande :

```bash
cd websocket-arosaje
npm i
node index.js
```

## Usage

Ce projet est dépendant du projet [backend-arosaje](https://gitlab.com/mspr-a-rosa-je1/back-end-arosaje).
